/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part4;

/**
 *
 * @author Admin
 */
class Square extends Shape {
  private double side;


  public Square(double side) {
    // Set the shape name as "Rectangle"
    
    this.side = side;

  }



  // Provide an implementation for inherited abstract getArea() method
  public double getArea() {
    return side * side;
  }

  // Provide an implementation for inherited abstract getPerimeter() method
  public double getPerimeter() {
    return 4.0 * side;
  }

    @Override
    public void draw() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



    }

