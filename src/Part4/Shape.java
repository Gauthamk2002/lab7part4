/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part4;

/**
 *
 * @author Admin
 */
abstract class Shape {

  public abstract void draw();

  public abstract double getArea();

  public abstract double getPerimeter();
}
