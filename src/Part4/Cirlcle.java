/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Part4;

/**
 *
 * @author Admin
 */
class Circle extends Shape {
  private double radius;

  public Circle(double radius) {

    this.radius = radius;
  }



  // Provide an implementation for inherited abstract getArea() method
  public double getArea() {
    return Math.PI * radius * radius;
  }

  // Provide an implementation for inherited abstract getPerimeter() method
  public double getPerimeter() {
    return 2.0 * Math.PI * radius;
  }

    @Override
    public void draw() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
